#!/bin/bash

# Internal variables
OUT_CLUSTER_SCOPED="cluster-scoped"
OUT_NAMESPACE_SCOPED="namespace-scoped"
TEMPLATE_RESULT="template-export.yaml"
EMPTY_REGEX="^$"

if ! [ -x "$(command -v kubernetes-split-yaml)" ]; then
  echo 'Error: kubernetes-split-yaml is not installed and needed. Please install kubernetes-split-yaml.' >&2
  exit 1
fi

# Setup Helm
function setup_helm() {
  HELM_CHART_HOME=$1
  helm repo add external-secrets ${HELM_CHART_HOME}
  helm repo update
}

function render_template() {
  chart="$1"
  release_name="$2"
  namespace="$3"
  template_result=${4:-$TEMPLATE_RESULT}

  helm template ${chart} \
    --name-template "${release_name}" \
    --set env.GOOGLE_APPLICATION_CREDENTIALS="/app/gcp-creds/config.json" \
    --set filesFromSecret.gcp-creds.secret="gcp-creds" \
    --set env.LOG_MESSAGE_KEY="message" \
    --set filesFromSecret.gcp-creds.mountPath="/app/gcp-creds" \
    --namespace="${namespace}" > ${template_result}

}

function split_template() {
  namespace=$1
  kubernetes-split-yaml --namespace_re="${EMPTY_REGEX}" --outdir "${OUT_CLUSTER_SCOPED}/cluster" ${TEMPLATE_RESULT}
  kubernetes-split-yaml --namespace_re="^${namespace}$" --outdir "${OUT_NAMESPACE_SCOPED}/$namespace" ${TEMPLATE_RESULT}
}

function create_secret() {
    KEY="$1"
    VALUE="$2"
    FILE="${3-false}"
    PROJECT="${4-$PROJECT_ID}"
    gcloud secrets -q versions access latest --secret="${KEY}" 2> /dev/null
    if [[ $? > 0 ]]; then
        gcloud secrets create ${KEY} --replication-policy="automatic" --project=${PROJECT}
    fi
    if [[ "$FILE" == "false" ]]; then
        # Standard Input
        echo ${VALUE} | gcloud secrets versions add ${KEY} --project=${PROJECT} --data-file=-
    else
        # File reference
        gcloud secrets versions add ${KEY} --project=${PROJECT} --data-file=${VALUE}
    fi
}

# Removes a secret
function delete_secret() {
    KEY="$1"
    PROJECT="${2-$PROJECT_ID}"
    gcloud secrets delete ${KEY} --project=${PROJECT} --quiet
}

function store_service_account_key() {
  KEY=$1
  GSA=$2
  PROJECT=${3:-$PROJECT_ID}

  if [[ -z "${KEY}" ]]; then
    2&> echo "Storing service account key as secret, but did not supply a key name"
    exit 1
  fi
  if [[ -z "${GSA}" ]]; then
    2&> echo "Storing service account key as secret, but did not supply a GSA name"
    exit 1
  fi

  gcloud secrets -q versions access latest --secret="${KEY}" --project=${PROJECT} 2> /dev/null
  if [[ $? > 0 ]]; then
    # only create a new key if one hasn't been created and stored in a secret
    gcloud iam service-accounts keys create ./key.json \
      --iam-account ${GSA}@${PROJECT}.iam.gserviceaccount.com \
      --project=${PROJECT}

    create_secret "${KEY}" "key.json" "true"

    rm -rf ./key.json
  fi
}
