#!/bin/bash

# Pull version of KubeVirt operator and configuration, separate out into proper nomos-structred folders, commit

# Requirements
# =============
# wget
# jq
# https://github.com/mogensen/kubernetes-split-yaml

KRM_OUTPUT_BASE="out"
WORKDIR="$(pwd)/workdir"
LEAVE_DIRTY="true"
KUBEVIRT_INSTALL_FILE="kubevirt-install.yaml"
KUBEVIRT_CLUSTER_SCOPE_FILE="kubevirt-cluster-scoped-objects.yaml"
KUBEVIRT_NAMESPACE_SCOPE_FILE="kubevirt-namespace-scoped-objects.yaml"

function generate_final_file() {
    echo "Creating File: $1"
    # Overwrite combined output file; create if not already
    echo "" > $1

    # Create the Cluster Scoped file
    FILES=( $( ls ${WORKDIR}/${KRM_OUTPUT_BASE}/$2/*.yaml ) )
    for FILE in "${FILES[@]}"; do
        cat $FILE >> $1
        echo -e "\n---\n" >> $1
    done

}

# Get the latest version if not defined by existing KUBEVIRT_VERSION env var
KUBEVIRT_VERSION=${KUBEVIRT_VERSION:-$(curl -s https://api.github.com/repos/kubevirt/kubevirt/releases/latest | jq -r .tag_name)}

echo "Using KubeVirt Version: $KUBEVIRT_VERSION"

# Create output file
if [[ ! -d ./${WORKDIR} ]]; then
    mkdir -p ${WORKDIR}
fi

if [[ ! -f "${WORKDIR}/${KUBEVIRT_INSTALL_FILE}" ]]; then
    wget --output-document=${WORKDIR}/${KUBEVIRT_INSTALL_FILE} https://github.com/kubevirt/kubevirt/releases/download/${KUBEVIRT_VERSION}/kubevirt-operator.yaml
fi

if [[ ! -d ./${KRM_OUTPUT_BASE} ]]; then
    kubernetes-split-yaml --template_sel tpl_ns --outdir ${WORKDIR}/${KRM_OUTPUT_BASE} ${WORKDIR}/${KUBEVIRT_INSTALL_FILE}
fi

# Remove "namespace" file in _no_ns_ (namespace file already exists in /config/namespace/kubevirt/namespace.yaml)
if [[ -f "${WORKDIR}/${KRM_OUTPUT_BASE}/_no_ns_/kubevirt.Namespace.yaml" ]]; then
    rm -rf ${WORKDIR}/${KRM_OUTPUT_BASE}/_no_ns_/kubevirt.Namespace.yaml
fi

generate_final_file "${WORKDIR}/${KUBEVIRT_CLUSTER_SCOPE_FILE}" "_no_ns_"
generate_final_file "${WORKDIR}/${KUBEVIRT_NAMESPACE_SCOPE_FILE}" "kubevirt"

cp ${WORKDIR}/${KUBEVIRT_CLUSTER_SCOPE_FILE} ../config/cluster/
cp ${WORKDIR}/${KUBEVIRT_NAMESPACE_SCOPE_FILE} ../config/namespaces/kubevirt/

if [[ "true" != "${LEAVE_DIRTY}" ]]; then
    rm -rf ${WORKDIR}
fi