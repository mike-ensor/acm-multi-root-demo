#!/bin/bash

# Pull version of CDI operator and configuration, separate out into proper nomos-structred folders, commit

# Requirements
# =============
# wget
# jq
# https://github.com/mogensen/kubernetes-split-yaml

unset CURR_DIR
unset PREFIX_DIR

KRM_OUTPUT_BASE="out"
WORKDIR="$(pwd)/workdir"
LEAVE_DIRTY="true"
CDI_OPERATOR_INSTALL_FILE="cdi-operator-install.yaml"
CDI_CR_INSTALL_FILE="cdi-cr-install.yaml"
CDI_CLUSTER_SCOPE_FILE="cdi-cluster-scoped-objects.yaml"
CDI_NAMESPACE_SCOPE_FILE="cdi-namespace-scoped-objects.yaml"

function generate_final_file() {
    echo "Creating File: $1"
    # Overwrite combined output file; create if not already
    echo "" > $1

    # Create the Cluster Scoped file
    FILES=( $( ls ${WORKDIR}/${KRM_OUTPUT_BASE}/$2/*.yaml ) )
    for FILE in "${FILES[@]}"; do
        cat $FILE >> $1
        echo -e "\n---\n" >> $1
    done

}

# Variables used to determine if running from the project root folder or within /extras
CURR_DIR=$(pwd)
PREFIX_DIR="./"

# Get the latest version if not defined by existing KUBEVIRT_VERSION env var
VERSION=$(curl -s https://github.com/kubevirt/containerized-data-importer/releases/latest | grep -o "v[0-9]\.[0-9]*\.[0-9]*")
echo "Using CDI Version: $VERSION"

if [[ "${CURR_DIR}" == *"/extras"* ]]; then
    # Script is run from within the extras folder
    PREFIX_DIR="../"
fi

# Create output file
if [[ ! -d ./${WORKDIR} ]]; then
    mkdir -p ${WORKDIR}
fi

if [[ ! -f "${WORKDIR}/${CDI_INSTALL_FILE}" ]]; then
    wget --output-document=${WORKDIR}/${CDI_OPERATOR_INSTALL_FILE} https://github.com/kubevirt/containerized-data-importer/releases/download/$VERSION/cdi-operator.yaml
    wget --output-document=${WORKDIR}/${CDI_CR_INSTALL_FILE} https://github.com/kubevirt/containerized-data-importer/releases/download/$VERSION/cdi-cr.yaml
fi

if [[ ! -d ./${KRM_OUTPUT_BASE} ]]; then
    # Split up the operator ( #TODO: Is this necessary? )
    kubernetes-split-yaml --template_sel tpl_ns --outdir ${WORKDIR}/${KRM_OUTPUT_BASE} ${WORKDIR}/${CDI_OPERATOR_INSTALL_FILE}
    # Split up the CR
    kubernetes-split-yaml --template_sel tpl_ns --outdir ${WORKDIR}/${KRM_OUTPUT_BASE} ${WORKDIR}/${CDI_CR_INSTALL_FILE}
fi

# Remove "namespace" file in _no_ns_ (namespace file already exists in /config/namespace/cdi/namespace.yaml)
if [[ -f "${WORKDIR}/${KRM_OUTPUT_BASE}/_no_ns_/cdi.Namespace.yaml" ]]; then
    rm -rf ${WORKDIR}/${KRM_OUTPUT_BASE}/_no_ns_/cdi.Namespace.yaml
fi

generate_final_file "${WORKDIR}/${CDI_CLUSTER_SCOPE_FILE}" "_no_ns_"
generate_final_file "${WORKDIR}/${CDI_NAMESPACE_SCOPE_FILE}" "cdi"

cp ${WORKDIR}/${CDI_CLUSTER_SCOPE_FILE} ${PREFIX_DIR}/config/cluster/
cp ${WORKDIR}/${CDI_NAMESPACE_SCOPE_FILE} ${PREFIX_DIR}/config/namespaces/cdi/

if [[ "true" != "${LEAVE_DIRTY}" ]]; then
    rm -rf ${WORKDIR}
fi