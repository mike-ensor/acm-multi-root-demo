# Overview

This is an example working git repository for Anthos Config Management enabled for Multi-Repo configuration

# Setup/Use

1. Fork this repo
1. Create a "Personal Access Token" (https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token)
    1. Create using "API" access
    1. (Optional) Set a short-lived expire date
    1. Suggest using "`acm-access-user`" for the "Name"
    1. Save the `token` and `user` in environment variables (and good idea to save in text later)
        ```bash
        export SCM_TOKEN_USER="<whatever name you gave it>"
        export SCM_TOKEN_TOKEN="<whatever your token value is>"
        ```
1. Create a GKE cluster
1. Install the ACM operator (need Anthos enabled project and IAM)
    1. Download the operator
        ```bash
        gsutil cp gs://config-management-release/released/1.7.0/config-management-operator.yaml config-management-operator.yaml
        ```
    1. Install the opeator
        ```bash
        kubectl apply -f config-management-operator.yaml
        ```
    1. Verify installation
        ```
        kubectl wait --for=condition=established --timeout=600s crd configmanagements.configmanagement.gke.io
        ```
1. Setup the `ConfigManagement`
    1. Create file called `config-management.yaml`
        ```yaml
        apiVersion: configmanagement.gke.io/v1
        kind: ConfigManagement
        metadata:
          name: config-management
          namespace: config-management-system
        spec:
          # Unique across all clusters using the same ConfigSync repo. Name is used in Cluster Selectors
          clusterName: {{ acm_cluster_name }}
          enableMultiRepo: true
        ```
    1. Install the ConfigManagement CR
        ```bash
        kubectl apply -f config-management.yaml
        ```
    1. Verify installation (sets up "rootsyncs" CRD)
        ```
        kubectl wait --for=condition=established --timeout=600s crd rootsyncs.configsync.gke.io
        ```
1. Create `git-creds` secret in `config-mangaement-system` namespace for `RootSync` CRD to use
    1. Create the k8s secret
        ```bash
        kubectl create secret generic git-creds --from-literal=username=${SCM_TOKEN_USER} --from-literal=token=${SCM_TOKEN_TOKEN} --namspace="config-management-system"
        ```
1. Configure `RootSync` to use this repo
    1. Create file called `root-sync.yaml` and replace `{{ acm_root_repo }}` with the URL of this forked repo
        ```yaml
        # root-sync.yaml
        apiVersion: configsync.gke.io/v1alpha1
        kind: RootSync
        metadata:
          name: root-sync
          namespace: config-management-system
        spec:
          sourceFormat: hierarchy
          git:
            repo: "{{ acm_root_repo }}"
            branch: "main"
            dir: "/config"
            auth: "token"
            secretRef:
              name: "git-creds"

        ```
    1. Install the ConfigManagement CR
        ```bash
        kubectl apply -f config-management.yaml
        ```
    1. Verify installation (sets up "rootsyncs" CRD)
        ```
        kubectl wait --for=condition=established --timeout=600s crd rootsyncs.configsync.gke.io
        ```
1. Check installation worked by looking for namespaces
    1. Check `utiliies` namespace exists. `utilities` is always going to be on a cluster and will not contain a `ClusterSelector`.
        ```bash
        kubectl get ns utilities
        ```
    1. If this fails, the ACM has not been installed or configured properly.

---


## Obtaining Cluster Viewer Token (login to cluster)

```bash
# 'console-cluster-reader' is a KSA defined in /config/namespaces/default/console-cluster-reader-sa.yaml
SECRET_NAME=$(kubectl get serviceaccount console-cluster-reader -o jsonpath='{$.secrets[0].name}')

TOKEN=$(kubectl get secret ${SECRET_NAME} -o jsonpath='{$.data.token}' | base64 --decode)

echo -e "\n\n${TOKEN}\n\n"

```

# Adding selector

```yaml
...
...
metadata:
  name: some-krm-name
  annotations:
    configmanagement.gke.io/cluster-selector: name-of-selector
...
...
```

# External Secrets

Need to create a GSA and `Secret` to enable `ExternalSecret`s to pull secrets